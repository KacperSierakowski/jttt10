﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mime;
using System.Net;

namespace JakToToTo
{
    class Wysylam
    {

        public void wyslij(String moj_mail, String haslo)
        {
            MailMessage mail = new MailMessage();
            mail.AlternateViews.Add(getEmbeddedImage(Application.StartupPath + @"\image.jpg"));
            mail.From = new MailAddress(moj_mail);
            mail.To.Add(moj_mail);
            mail.Subject = "Obrazek";
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(moj_mail, haslo);
            smtp.Host = "smtp.gmail.com";
            smtp.Send(mail);
        }
        private AlternateView getEmbeddedImage(String filePath)
        {
            LinkedResource inline = new LinkedResource(filePath);
            inline.ContentId = Guid.NewGuid().ToString();
            string htmlBody = @"<img src='cid:" + inline.ContentId + @"'/>";
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(inline);
            return alternateView;
        }

    }
}
