﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using HtmlAgilityPack;

namespace JakToToTo
{
    class Szukam
    {
        public string _url = "http://demotywatory.pl/";
        public string Poszukiwany_Src;//adres pliku ze zdjeciem
        public List<string> tab_src2 = new List<string>();
        public List<string> tab_alt2 = new List<string>();
        
        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));
                return html;
            }
        }
        
        public void PrintPageNodes()
        {
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml();
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");
            
            foreach (var node in nodes)
            {
                tab_src2.Add(node.GetAttributeValue("src", ""));
                tab_alt2.Add(node.GetAttributeValue("alt", ""));
             }
        }

        public string Czy_Zawieram(string haslo)
        {
            for(int i = 0; i < tab_src2.Count; i++)
            {
                if (tab_alt2[i].Contains(haslo))
                {
                    Poszukiwany_Src = tab_src2[i];
                }
            }
            return Poszukiwany_Src;
        }


        public void Clean_Obrazki()
        {
            tab_src2.Clear();
            tab_alt2.Clear();
        }

    }
}
